# Embark Front End Kitty Kit

No Kitties sadly.

![Sad Kitty](http://cdn.cutestpaw.com/wp-content/uploads/2013/10/l-Sad-kitten.jpg)

## Installation

So easy your grandma could do it.

Essentially this becomes the root of your website, with the `/public` folder being the web-root. Just `git clone` the project to where ever you want it to be.

```
git clone git@bitbucket.org:embarknow/kitty-kit.git
```

Once that's done, `cd` into the directory, and run

```
npm install && bower install
```

And you're ready to go.

## Usage

All project stuff, like assets, will be stored under the `/app` folder. Bower will install any components to `/app/assets/bower` and these will be minified using `gulp` during development.

Just run `gulp` from the command line during development to automatically process assets.
