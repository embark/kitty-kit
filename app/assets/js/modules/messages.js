/**
 * messages.js
 * Deals with message banners
 */

var $ = require('jquery');

module.exports = function () {
    var wrapper = $('.messages'),
        messages = wrapper.find('aside.message');

    messages.each(function () {
        var message = $(this),
            close = message.find('a.close');

        close.on('click', function (ev) {
            message.remove();
        });
    });
};
