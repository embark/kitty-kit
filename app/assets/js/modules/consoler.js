// Console fallback for older browsers
module.exports = function () {
    if (typeof global.console === 'undefined') {
        global.console = {};
        global.console.log = global.console.error = global.console.info = global.console.debug = global.console.warn = global.console.trace = global.console.dir = global.console.dirxml = global.console.group = global.console.groupEnd = global.console.time = global.console.timeEnd = global.console.assert = global.console.profile = function () {};
    }
};
