## Folder Structure

The following is the folder structure of the sass project. The layout, modules and vendor folders will change dependant on each projects requirements.

```
styles.scss                 // Glue it all together
/_config
    /_colors.scss           // All project colours as variables
    /_extends.scss          // Classes to only be extended
    /_fonts.scss            // Font face declarations
    /_helpers.scss          // Helper functions and mixins
    /_icons.scss            // Setup for icons and icon fonts
    /_reset.scss            // Reset the browser to a standard default
    /_variables.scss        // Re-useable variables
/base
    /_buttons.scss          // Buttons and links
    /_elements.scss         // Basics for most HTML elements
    /_forms.scss            // Form normalising
    /_lists.scss            // List styling
    /_media.scss            // Any media elements
    /_tables.scss           // Tables
/layout
    /_breakpoints.scss      // Catalogue of in-use breakpoints
    /_flexgrid.scss         // Simple implementation of flexgrid. Fails in <=IE9
    /_lobotomised_owl.scss  // http://alistapart.com/article/axiomatic-css-and-lobotomized-owls
    /_neatgrid.scss         // Settings for Bourbon Neat grig
    /_scaffolding.scss      // Scaffolding layout for the site wrappers and structure
/modules
    /...                    // Modules include footers, headers, navigation, etc
/vendor
    /bourbon                // Bourbon scss framework
    /font-awesome           // Font awesome for icon prototyping
    /neat                   // Neat for grids and grid helpers
```

## Order of Import

Each file is imported in a specific order to maintain dependencies, and also try to maintain an order of specificity (at least to a certain degree).

```
vendor/bourbon
vendor/font-awesome

_config/variables
_config/fonts
_config/icons
_config/colors
_config/helpers
_config/extends
_config/reset

layout/flexgrid (or layout/neatgrid)
layout/breakpoints
layout/lobotomised_owl
layout/scaffolding

base/elements
base/lists
base/tables
base/media
base/forms
base/buttons

layout/... (any other layouts)
modules/... (any modules)
```
