/**
 * Config
 */

var src = './app/assets',
    dest = './public/assets';

module.exports = {
    paths: {
        src: src,
        dest: dest
    },
    browserify: {
        debug: true,
        bundles: [{
            entries: src + '/js/app.js',
            dest: dest + '/js',
            name: 'app.js'
        }, {
            entries: src + '/js/head.js',
            dest: dest + '/js',
            name: 'head.js'
        }]
    },
    fonts: {
        src: src + '/fonts/**',
        dest: dest + '/fonts'
    },
    images: {
        src: src + '/img/**',
        dest: dest + '/img'
    },
    bower: {
        src: src + '/bower/*',
        dest: dest + '/js',
        name: 'vendor.js'
    },
    plugins: {
        src: [
            src + '/js/plugins/**',
        ],
        dest: dest + '/js',
        name: 'plugins.js'
    },
    lint: {
        src: src + '/js/*'
    },
    polyfills: {
        src: src + '/js/polyfills/*!(min).js',
        dest: dest + '/js',
        name: 'polyfills.js'
    },
    sass: {
        src: src + '/sass/styles.scss',
        dest: dest + '/css',
        name: 'styles.css'
    }
};
