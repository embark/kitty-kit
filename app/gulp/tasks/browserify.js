/**
 * Browserify Task
 */

var browserify      = require('browserify'),
    config          = require('../config').browserify,
    gulp            = require('gulp'),
    errorHandler    = require('../utils/errorHandler'),
    logger          = require('../utils/logger'),
    source          = require('vinyl-source-stream'),
    buffer          = require('vinyl-buffer'),
    watchify        = require('watchify'),
    uglify          = require('gulp-uglify');

gulp.task('browserify', function (callback) {
    // Only run if configured
    var queue = config.bundles.length,
        run = function (item) {
            var bundler = browserify({
                    cache: {},
                    packageCache: {},
                    fullPaths: true,
                    entries: item.entries,
                    debug: config.debug
                }),
                finished = function () {
                    logger.end(item.name);

                    if (queue) {
                        queue --;

                        if (queue === 0) {
                            callback();
                        }
                    }
                },
                bundle = function () {
                    logger.start(item.name);

                    return bundler
                        .bundle()
                        .on('error', errorHandler)
                        .pipe(source(item.name))
                        .pipe(buffer())
                        .pipe(uglify())
                        .pipe(gulp.dest(item.dest))
                        .on('end', finished);
                };

            if (global.isWatching) {
                bundler = watchify(bundler);
                bundler.on('update', bundle);
            }

            return bundle();
        };

        config.bundles.forEach(run);
});

gulp.task('browserify-dev', function (callback) {
    // Only run if configured
    var queue = config.bundles.length,
        run = function (item) {
            var bundler = browserify({
                    cache: {},
                    packageCache: {},
                    fullPaths: true,
                    entries: item.entries,
                    debug: config.debug
                }),
                finished = function () {
                    logger.end(item.name);

                    if (queue) {
                        queue --;

                        if (queue === 0) {
                            callback();
                        }
                    }
                },
                bundle = function () {
                    logger.start(item.name);

                    return bundler
                        .bundle()
                        .on('error', errorHandler)
                        .pipe(source(item.name))
                        .pipe(buffer())
                        .pipe(gulp.dest(item.dest))
                        .on('end', finished);
                };

            if (global.isWatching) {
                bundler = watchify(bundler);
                bundler.on('update', bundle);
            }

            return bundle();
        };

        config.bundles.forEach(run);
});
