/**
 * Watch - Watch for changes
 */

var gulp   = require('gulp'),
    gutil  = require('gulp-util'),
    config = require('../config');

gulp.task('watch', function () {
    gulp.watch(config.paths.src + '/js/**/*.js', ['lint', 'bower-dev', 'plugins-dev', 'browserify-dev']);
    gulp.watch(config.paths.src + '/img/**', ['images']);
    gulp.watch(config.paths.src + '/sass/**/*.scss', ['sass-dev']);
});
