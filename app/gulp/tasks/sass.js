/**
 * Sass - Output CSS from Sass files
 */

var gulp   = require('gulp'),
    gutil  = require('gulp-util'),
    config = require('../config').sass,
    sass   = require('gulp-ruby-sass');

gulp.task('sass', function () {
    return gulp.src(config.src)
        .pipe(sass({noCache: true, style: "compressed"})
            .on('error', gutil.log)
            .on('error', gutil.beep))
        .pipe(gulp.dest(config.dest));
});

gulp.task('sass-dev', function () {
    return gulp.src(config.src)
        .pipe(sass({sourcemap: true, noCache: true, style: "expanded"})
            .on('error', gutil.log)
            .on('error', gutil.beep))
        .pipe(gulp.dest(config.dest));
});
