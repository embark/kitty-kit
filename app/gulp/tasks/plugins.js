/**
 * Scripts - Gather, concatenate and minify script files
 */

var gulp   = require('gulp'),
    gutil  = require('gulp-util'),
    concat = require('gulp-concat'),
    config = require('../config').plugins,
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify');

gulp.task('plugins', function () {
    return gulp.src(config.src)
        .pipe(concat(config.name))
        .pipe(gulp.dest(config.dest))
        .pipe(uglify()
            .on('error', gutil.log)
            .on('error', gutil.beep))
        .pipe(gulp.dest(config.dest));
});

gulp.task('plugins-dev', function () {
    return gulp.src(config.src)
        .pipe(concat(config.name))
        .pipe(gulp.dest(config.dest));
});
