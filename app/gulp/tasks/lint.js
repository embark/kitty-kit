/**
 * Lint - Run all JS files through JSHint
 */

var gulp   = require('gulp'),
    config = require('../config').lint,
    jshint = require('gulp-jshint');

gulp.task('lint', function () {
    return gulp.src(config.src)
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});
