/**
 * Images - Runs images through imagemin only when the source has changed
 */

var gulp     = require('gulp'),
    gutil    = require('gulp-util'),
    changed  = require('gulp-changed'),
    config   = require('../config').images,
    imagemin = require('gulp-imagemin');

gulp.task('images', function () {
    return gulp.src(config.src)
        .pipe(changed(config.dest))
        .pipe(imagemin()
            .on('error', gutil.log)
            .on('error', gutil.beep))
        .pipe(gulp.dest(config.dest));
});
