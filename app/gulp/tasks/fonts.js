/**
 * Fonts - Copy all font files to destintation
 */

var gulp     = require('gulp'),
    gutil    = require('gulp-util'),
    changed  = require('gulp-changed'),
    config   = require('../config').fonts;

gulp.task('fonts', function () {
    return gulp.src(config.src)
        .pipe(gulp.dest(config.dest));
});
