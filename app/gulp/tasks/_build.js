var gulp = require('gulp');

gulp.task('build', ['lint', 'sass', 'bower', 'browserify', 'plugins', 'polyfills', 'images', 'fonts']);

gulp.task('build-dev', ['lint', 'sass-dev', 'bower-dev', 'browserify-dev', 'plugins-dev', 'polyfills-dev', 'images', 'fonts']);
