/**
 * Scripts - Gather, concatenate and minify script files
 */

var gulp   = require('gulp'),
    gutil  = require('gulp-util'),
    concatVendor = require('gulp-concat-vendor'),
    config = require('../config').bower,
    uglify = require('gulp-uglify');

gulp.task('bower', function () {
    return gulp.src(config.src)
        .pipe(concatVendor(config.name))
        .pipe(uglify()
            .on('error', gutil.log)
            .on('error', gutil.beep))
        .pipe(gulp.dest(config.dest));
});

gulp.task('bower-dev', function() {
    return gulp.src(config.src)
        .pipe(concatVendor(config.name))
        .pipe(gulp.dest(config.dest));
});
