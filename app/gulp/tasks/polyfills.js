/**
 * Polyfills - Gather, concatenate and minify polyfill files
 */

var gulp   = require('gulp'),
    gutil  = require('gulp-util'),
    concat = require('gulp-concat'),
    config = require('../config').polyfills,
    uglify = require('gulp-uglify');

gulp.task('polyfills', function () {
    return gulp.src(config.src)
        .pipe(concat(config.name))
        .pipe(uglify()
            .on('error', gutil.log)
            .on('error', gutil.beep))
        .pipe(gulp.dest(config.dest));
});

gulp.task('polyfills-dev', function () {
    return gulp.src(config.src)
        .pipe(concat(config.name))
        .pipe(gulp.dest(config.dest));
});
